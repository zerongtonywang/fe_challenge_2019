#Biarri front end challenge

This is Biarri's challenge for front end candidates. We'd like you to spend around a max of two hours on this solution.
Please fork this repo for development, drop us an email when you start working on the solution and when you have finished the challenge send us a link to your repo! 

## Guidelines

You�ve been hired by Biarri Groceries to develop an employee management solution for their staff. Based on the mockup provided, your tasks are the following:

- Implement the layout and data table as shown in the mockup (assets/mockup.png) - ensure you develop exactly to the design provided.
- Implement the filter shown in the mockup. The filter should be a string �contains� style search, and be usable with all columns.
- Populate the table with the JSON set provided (assets/data.json)
- Using your UX skills, design and implement in the application a user-friendly way to:

   - Add new rows to the table
  - Edit table data inline
   - Change the status of an employee.

The application should be built using React, and does not require any persistence of changes to the data set. Please avoid using a component for the data table.
