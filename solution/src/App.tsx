import React, { useState, useRef, useEffect } from "react";
import { useStyles } from "./App.style";
import data from "./data.json";
import { ReactComponent as SearchSvg } from "./search.svg";
import { CellInput } from "./CellInput";

function getStatus(id: number) {
  return data.employment_statuses.find(status => status.id === id);
}

export function App() {
  const styles = useStyles();
  const [search, setSearch] = useState("");
  const [employees, setEmployees] = useState(data.employees);
  const [prevEmployees, setPrevEmployees] = useState(employees);
  const newFocusRef: React.RefObject<HTMLInputElement> = useRef(null);

  useEffect(() => {
    if (employees.length - prevEmployees.length === 1) {
      if (newFocusRef.current) {
        newFocusRef.current.focus();
      }
    }
    setPrevEmployees(employees);
  }, [employees, prevEmployees.length]);

  const filteredEmployees = employees.filter((employee: any) => {
    return (
      !search ||
      Object.keys(employee).some(key =>
        String(employee[key])
          .toLowerCase()
          .includes(search)
      )
    );
  });

  function handleChange(
    event: React.ChangeEvent<HTMLInputElement>,
    rowIndex: number,
    key: string
  ) {
    const emploeesCopy = JSON.parse(JSON.stringify(employees));
    emploeesCopy[rowIndex][key] = event.currentTarget.value;
    setEmployees(emploeesCopy);
  }

  function handleAddNew() {
    setEmployees([
      ...employees,
      {
        id: (Math.max(...employees.map(employee => employee.id)) || 0) + 1,
        first_name: "",
        last_name: "",
        date_of_birth: "",
        employment_status: 1,
        date_hired: ""
      }
    ]);
  }

  function handleChangeStatus(rowIndex: number) {
    const emploeesCopy = JSON.parse(JSON.stringify(employees));
    emploeesCopy[rowIndex].employment_status =
      (emploeesCopy[rowIndex].employment_status + 1) % 3 || 3;
    setEmployees(emploeesCopy);
  }

  return (
    <div className={styles.root}>
      <div className={styles.blueBg} />
      <div className={styles.content}>
        <span className={styles.title}>BIARRI</span>
        <span className={styles.title}>GROCERIES</span>
        <div className={styles.card}>
          <div className={styles.cardHeader}>
            <SearchSvg className={styles.searchIcon} />
            <input
              className={styles.input}
              value={search}
              onChange={e => setSearch(e.currentTarget.value.toLowerCase())}
              placeholder="Search for..."
            />
          </div>
          <div className={styles.cardContent}>
            <table className={styles.table}>
              <thead>
                <tr>
                  <td>ID</td>
                  <td>First Name</td>
                  <td>Last Name</td>
                  <td>Date Of Birth</td>
                  <td>Employment Status</td>
                  <td>Date Hired</td>
                </tr>
              </thead>
              <tbody>
                {filteredEmployees.map((row, index) => (
                  <tr key={row.id}>
                    <td>
                      <CellInput
                        onChange={event => handleChange(event, index, "id")}
                        value={String(row.id)}
                      />
                    </td>
                    <td>
                      <CellInput
                        forwardedRef={
                          index === filteredEmployees.length - 1
                            ? newFocusRef
                            : undefined
                        }
                        onChange={event =>
                          handleChange(event, index, "first_name")
                        }
                        value={row.first_name}
                      />
                    </td>
                    <td>
                      <CellInput
                        onChange={event =>
                          handleChange(event, index, "last_name")
                        }
                        value={row.last_name}
                      />
                    </td>
                    <td>
                      <CellInput
                        onChange={event =>
                          handleChange(event, index, "date_of_birth")
                        }
                        value={row.date_of_birth}
                      />
                    </td>
                    <td className={styles.chipTd}>
                      <div
                        className={styles.chip}
                        style={{
                          backgroundColor: getStatus(row.employment_status)!
                            .colour
                        }}
                      >
                        {getStatus(row.employment_status)!.name}
                      </div>
                      <button
                        className={styles.statusButton}
                        onClick={() => handleChangeStatus(index)}
                      >
                        change
                      </button>
                    </td>
                    <td>
                      <CellInput
                        rightAligned
                        onChange={event =>
                          handleChange(event, index, "date_hired")
                        }
                        value={row.date_hired}
                      />
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>

            <div className={styles.actions}>
              <button className={styles.newButton} onClick={handleAddNew}>
                Add New Employee
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
