import { makeStyles } from "@material-ui/styles";

export const useStyles = makeStyles({
  root: {
    color: "inherit",
    fontSize: "inherit",
    border: "none",
    padding: 12,
    maxWidth: "100%",
    boxSizing: "border-box",
    "&:hover:not(:focus)": {
      outline: "1px solid rgba(0, 0, 0, 0.2)"
    }
  },
  rootRightAligned: {
    textAlign: "right"
  }
});
