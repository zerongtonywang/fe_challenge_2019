import classNames from "classnames";
import React from "react";
import { useStyles } from "./CellInput.styles";

interface Props {
  value: string;
  forwardedRef?: React.RefObject<HTMLInputElement>;
  onChange?: (event: React.ChangeEvent<HTMLInputElement>) => void;
  rightAligned?: boolean;
}

export function CellInput({
  rightAligned,
  forwardedRef,
  ...otherProps
}: Props) {
  const styles = useStyles();
  return (
    <input
      ref={forwardedRef}
      className={classNames(styles.root, {
        [styles.rootRightAligned]: rightAligned
      })}
      {...otherProps}
    />
  );
}
