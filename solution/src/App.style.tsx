import { makeStyles } from "@material-ui/styles";

export const useStyles = makeStyles({
  "@global": {
    body: {
      margin: 0,
      fontFamily: "'Roboto', sans-serif"
    }
  },
  root: {
    position: "relative"
  },
  blueBg: {
    width: "100%",
    height: "50%",
    position: "absolute",
    bottom: 0,
    backgroundColor: "#A5E7FF",
    zIndex: -1
  },
  content: {
    minHeight: "100vh",
    boxSizing: "border-box",
    padding: 40,
    display: "flex",
    flexDirection: "column"
  },
  title: {
    fontSize: 24,
    lineHeight: 1.5,
    fontWeight: 900,
    letterSpacing: 12
  },
  card: {
    backgroundColor: "white",
    flex: "1 1 auto",
    boxShadow:
      "0 10px 20px rgba(0, 0, 0, 0.19), 0 6px 6px rgba(0, 0, 0, 0.23);",
    marginTop: 30,
    borderRadius: 20
  },
  cardHeader: {
    padding: "80px 52px 16px",
    borderBottom: "1px solid rgba(0, 0, 0, 0.2)",
    display: "flex",
    alignItems: "flex-end"
  },
  searchIcon: {
    height: 32,
    width: 32,
    fill: "rgba(0, 0, 0, 0.5)",
    marginLeft: -2
  },
  input: {
    marginLeft: 12,
    flex: "1 1 auto",
    border: "none",
    fontSize: 24,
    "&:focus": {
      outline: "none"
    }
  },
  cardContent: {
    padding: "52px"
  },
  table: {
    width: "100%",
    tableLayout: "fixed",
    "& td": {
      fontSize: 18
    },
    "& thead td": {
      fontWeight: 500,
      padding: "0 12px 24px"
    },
    "& thead td:last-child": {
      textAlign: "right",
      paddingRight: 12
    },
    "& tbody td": {
      height: 80,
      boxSizing: "border-box",
      paddingRight: 20,
      color: "rgba(0, 0, 0, 0.5)"
    },
    "& tbody td:last-child": {
      paddingRight: 0,
      textAlign: "right"
    }
  },
  chipTd: {
    display: "flex",
    alignItems: "center",
    paddingLeft: 12
  },
  chip: {
    color: "white",
    fontWeight: 500,
    borderRadius: 20,
    padding: "8px 20px"
  },
  statusButton: {
    border: "none",
    color: "blue",
    cursor: "pointer",
    marginLeft: 8,
    "&:hover": {
      textDecoration: "underline"
    }
  },
  actions: {
    margin: "40px 0",
    textAlign: "center"
  },
  newButton: {
    border: "none",
    fontSize: 18,
    color: "blue",
    cursor: "pointer",
    padding: "12px 20px",
    "&:hover": {
      textDecoration: "underline"
    }
  }
});
